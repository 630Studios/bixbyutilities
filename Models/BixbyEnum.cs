﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BixbyUtilities.Models
{
    public class BixbyEnum
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public List<string> Values { get; set; } = new List<string>();
    }
}
