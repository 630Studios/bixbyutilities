﻿using System;
using System.Collections.Generic;
using System.Text;
using BixbyUtilities.Models;

namespace BixbyUtilities.Extensions
{
    public static class BixbyVocabExtensions
    {
        public static BixbyVocab.VocabEntry AddEntry(this BixbyVocab vocab, string name)
        {
            BixbyVocab.VocabEntry entry = new BixbyVocab.VocabEntry { Name = name };
            entry.Alternatives.Add(name);
            vocab.Entries.Add(entry);
            return entry;
        }

    }
}
