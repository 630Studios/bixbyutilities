﻿using System.Collections.Generic;
using System.Linq;
using BixbyUtilities.Models;

namespace BixbyUtilities.Convertors
{
    /// <summary>
    /// Holds the results of the Conversion Process.
    /// </summary>
    public class ConversionResults
    {
        public List<BixbyStructure> GeneratedStructures { get; set; } = new List<BixbyStructure>();
        public List<BixbyPrimitive> GeneratedPrimitives { get; set; } = new List<BixbyPrimitive>();

        public List<BixbyEnum> Generatedenums { get; set; } = new List<BixbyEnum>();

        public List<BixbyVocab> GeneratedVocab { get; set; } = new List<BixbyVocab>();


        /// <summary>
        /// Checks if the results already contains a primitive with the given name.
        /// </summary>
        /// <param name="name">Name of the primitive to check for.</param>
        /// <returns></returns>
        public bool HasPrimitive(string name)
        {
            return GeneratedPrimitives.Any(p => p.Name == name);
        }

        /// <summary>
        /// Checks if the results already contains a structure with the given name.
        /// </summary>
        /// <param name="name">Name of the structure to check for.</param>
        /// <returns></returns>
        public bool HasStructure(string name)
        {
            return GeneratedStructures.Any(p => p.Name == name);
        }

        /// <summary>
        /// Get a primitive from the results by name.
        /// </summary>
        /// <param name="name">Name of the primitive to get.</param>
        /// <returns>Returns the BixbyPrimitive if one is found, or null</returns>
        public BixbyPrimitive GetPrimitive(string name)
        {
            return GeneratedPrimitives.FirstOrDefault(p => p.Name == name);
        }

        /// <summary>
        /// Get a structure from the results by name.
        /// </summary>
        /// <param name="name">Name of the structure to get.</param>
        /// <returns>Returns the BixbyStructure if one is found, or null</returns>
        public BixbyStructure GetStructure(string name)
        {
            return GeneratedStructures.FirstOrDefault(p => p.Name == name);
        }

    }
}
