﻿using System;
using BixbyUtilities.Models;

namespace BixbyUtilities.Extensions
{

    /// <summary>
    /// Helper methods for the BixbyProperty class
    /// </summary>
    public static class BixbyPropertyExtensions
    {

        public static string GetBixbyOutput(this BixbyProperty property)
        {
            string minVal = property.Min.ToString();
            string maxVal = property.Max.ToString();
            string visVal = property.IsPublic ? "Public" : "Private";
            string propertyType = property.Primitive == null ? property.Structure.Name : property.Primitive.Name;

            string output = "\tproperty (" + property.Name + ") {" + Environment.NewLine +
                            "\t\ttype(" + propertyType + ")" + Environment.NewLine +
                            "\t\tmin(" + minVal +") max(" + maxVal + ")" + Environment.NewLine +
                            "\t\tvisibility(Public)" + Environment.NewLine +
                            "\t}" + Environment.NewLine + Environment.NewLine;

            return output;

        }
    }
}
