﻿using System.Linq;
using System.Reflection;
using BixbyUtilities;
namespace BixbyUtilities.Convertors
{

    public static class TypeConversionTable
    {
        public delegate bool TypeValidationDelegate(System.Type type);

        public class ConversionTableEntry
        {

            public string Name { get; set; }

            public BixbyTypes BixbyType { get; set; }


            public TypeValidationDelegate Validator { get; set; }
            public ConversionTableEntry(string name, BixbyTypes bixbyType, TypeValidationDelegate del)
            {
                this.Name = name;
                this.BixbyType = bixbyType;
                this.Validator = del;
            }
        }


        public static ConversionTableEntry[] Entries = new ConversionTableEntry[]
        {
            new ConversionTableEntry("Boolean",  BixbyTypes.BOOLEAN, ValidateBool),
            new ConversionTableEntry("Decimal", BixbyTypes.DECIMAL, ValidateDecimal),
            new ConversionTableEntry("Enum", BixbyTypes.ENUM, ValidateEnum),
            new ConversionTableEntry("Integer", BixbyTypes.INTEGER, ValidateInt),
            new ConversionTableEntry("Name", BixbyTypes.NAME, ValidateString), // All string default to 'name' type for now. Use of attributes on properties can override this.
            //new ConversionTableEntry("Qualified", BixbyTypes.QUALIFIED, typeof(bool)),
            //new ConversionTableEntry("Text", BixbyTypes.TEXT, typeof(bool)),
        };

        public static BixbyTypes GetBixbyType(System.Type type)
        {
            if (type.IsClass && typeof(string) != type)
                return BixbyTypes.STRUCTURE;

            if (type.IsEnum)
                return BixbyTypes.ENUM;

            ConversionTableEntry conversionTableEntry =  Entries.FirstOrDefault(p => p.Validator(type) == true);
            if (conversionTableEntry == null)
                return BixbyTypes.UNDEFINED;

            return conversionTableEntry.BixbyType;
        }


        public static bool ValidateBool(System.Type type)
        {
            return typeof(bool) == type;
        }

        public static bool ValidateInt(System.Type type)
        {
            return typeof(int) == type;
        }

        public static bool ValidateDecimal(System.Type type)
        {
            return (typeof(decimal) == type || typeof(float) == type);
        }

        public static bool ValidateString(System.Type type)
        {
            return typeof(string) == type;
        }

        public static bool ValidateEnum(System.Type type)
        {
            return type.IsEnum;
        }


    }
}