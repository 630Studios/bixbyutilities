﻿namespace BixbyUtilities.Models
{
    public class BixbyProperty
    {

        public BixbyStructure Owner { get; set; }
        public string Name { get; set; }

        public System.Type CSharpType { get; set; }
        public BixbyTypes BixbyType { get; set; }
        public PropertyMax Max { get; set; }
        public PropertyMin Min { get; set; }
        public BixbyPrimitive Primitive { get; set; }
        public BixbyStructure Structure { get; set; }

        public bool IsPublic { get; set; }
    }



}
