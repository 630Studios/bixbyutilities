﻿namespace BixbyUtilities
{
    public enum ActionTypes
    {
        Calculation = 1,
        Constructor = 2,
        Fetch = 3,
        Search = 4,
        BeginTransaction = 5,
        UpdateTransaction = 6,
        CancelTransaction = 7,
        Commit = 8,
        RefreshActivity = 9,
        UpdateActivity = 10,
        CancelActivity = 11,
        ArchiveLookup = 12
    }

    public enum PropertyMin
    {
        Optional = 1,
        Required = 2,
    }

    public enum PropertyMax
    {
        One = 1,
        Many = 2,
    }

    public enum BixbyTypes
    {
        UNDEFINED = 0,
        BOOLEAN = 1,
        DECIMAL = 2,
        ENUM = 3,
        INTEGER = 4,
        NAME = 5,
        QUALIFIED = 6,
        TEXT = 7,
        STRUCTURE = 8,
    }

    public enum StructureFeatureTypes
    {
        None = 0,
        Activity = 1,
        Preferable = 2,
        ProfileStored = 4,
        Transaction = 8,
        Transient = 16
    }

    public enum PrimitiveFeatureTypes
    {
        None = 0,
        ProfileStored = 1,
        RecognizedRange = 2,
        Transient = 4,
    }

}
