﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using BixbyUtilities.Convertors;
using BixbyUtilities.Models;

namespace BixbyUtilities.Extensions
{
    public static class ConversionResultExtensions
    {

        public static void SaveGeneratedData(this ConversionResults results, string exportFolder)
        {
            SaveGeneratedStructures(results, exportFolder);
            SaveGeneratedPrimitives(results, exportFolder);
        }

        public static void SaveGeneratedStructures(this ConversionResults results, string exportFolder)
        {
            for (int i = 0; i < results.GeneratedStructures.Count; i++)
            {
                BixbyStructure structure = results.GeneratedStructures[i];

                if (!CheckForStructureFolder(exportFolder, true))
                {
                    throw new SystemException("Export folder does not exist, and could not be created.");
                }
                else
                {
                    structure.SaveBixbyStructure(exportFolder + "/structures");
                }
            }
        }

        public static void SaveGeneratedPrimitives(this ConversionResults results, string exportFolder)
        {
            for (int i = 0; i < results.GeneratedPrimitives.Count; i++)
            {
                BixbyPrimitive primitive = results.GeneratedPrimitives[i];

                if (!CheckForPrimitiveFolder(exportFolder, true))
                {
                    throw new SystemException("Export folder does not exist, and could not be created.");
                }
                else
                {
                    if (primitive.Owner == null)
                    {
                        primitive.SaveBixbyPrimitive(exportFolder + "/primitives");
                    }
                    else
                    {
                        if (!Directory.Exists(exportFolder + "/primitives/" + primitive.Owner.Structure.Name))
                            Directory.CreateDirectory(exportFolder + "/primitives/" + primitive.Owner.Structure.Name);

                        primitive.SaveBixbyPrimitive(exportFolder + "/primitives/" + primitive.Owner.Structure.Name); 
                    }

                }
            }
        }


        


        private static bool CheckForStructureFolder(string rootFolder, bool shouldCreateRoot)
        {
            if (!Directory.Exists(rootFolder) && !shouldCreateRoot)
            {
                return false;
            }
            else
            {
                Directory.CreateDirectory(rootFolder);
            }

            if (!Directory.Exists(rootFolder + "/structures"))
            {
                Directory.CreateDirectory(rootFolder + "/structures");
            }

            return true;
        }

        private static bool CheckForPrimitiveFolder(string rootFolder, bool shouldCreateRoot)
        {
            if (!Directory.Exists(rootFolder) && !shouldCreateRoot)
            {
                return false;
            }
            else
            {
                Directory.CreateDirectory(rootFolder);
            }

            if (!Directory.Exists(rootFolder + "/primitives"))
            {
                Directory.CreateDirectory(rootFolder + "/primitives");
            }

            return true;
        }
    }
}
