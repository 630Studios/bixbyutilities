﻿using System.IO;
using System.Text;
using BixbyUtilities.Models;

namespace BixbyUtilities.Extensions
{
    public static class BixbyStrutureExtensions
    {

        public static string GetBixbyOutput(this BixbyStructure structure)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("structure(" + structure.Name + ") {");
            sb.AppendLine("\tdescription(" + structure.Description + "))");

            for (int i = 0; i < structure.Properties.Count; i++)
            {
                sb.Append(structure.Properties[i].GetBixbyOutput());
            }

            sb.AppendLine("}");
            return sb.ToString();
        }

        public static void SaveBixbyStructure(this BixbyStructure structure, string rootFolder)
        {

            if (!Directory.Exists(rootFolder))
            {
                throw new System.Exception("SaveBixbyStructure: Root folder does not exist");
            }
            string filePath = rootFolder + "/" + structure.Name + ".model.bxb";

            File.WriteAllText(filePath, structure.GetBixbyOutput());
        }
    }
}
