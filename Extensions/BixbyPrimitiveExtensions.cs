﻿using System;
using System.IO;
using BixbyUtilities.Models;

namespace BixbyUtilities.Extensions
{

    /// <summary>
    /// Helper methods for the Bixby Primitive class.
    /// </summary>
    public static class BixbyPrimitiveExtensions
    {

        /// <summary>
        /// Generates a Bixby Primitive Definition.
        /// </summary>
        /// <param name="primitive">Target Primitive</param>
        /// <returns></returns>
        public static string GetBixbyOutput(this BixbyPrimitive primitive)
        {
            
            string output = primitive.BixbyType.ToString().ToLower() + "(" + primitive.Name + ") {" + Environment.NewLine +
                "\tdescription(" + primitive.Description + ")" + Environment.NewLine +
                ((primitive.Features != PrimitiveFeatureTypes.None) ? primitive.Features.ToString() : "") + Environment.NewLine +
                "}" + Environment.NewLine;

            return output;

        }

        /// <summary>
        /// Saves a Bixby Primitive Definition to the specified location.
        /// </summary>
        /// <param name="primitive">Target Primitive</param>
        /// <param name="rootFolder">Destination Folder</param>
        public static void SaveBixbyPrimitive(this BixbyPrimitive primitive, string rootFolder)
        {

            if (!Directory.Exists(rootFolder))
            {
                throw new System.Exception("SaveBixbyPrimitive: Root folder does not exist");
            }

            string filePath = rootFolder + "/" + primitive.Name + ".model.bxb";

            File.WriteAllText(filePath, primitive.GetBixbyOutput());
        }
    }
}
