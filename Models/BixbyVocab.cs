﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BixbyUtilities.Models
{
    public class BixbyVocab
    {
        public class VocabEntry
        {
            public string Name;
            public List<string> Alternatives { get; set; } = new List<string>();
        }

        public string Name { get; set; }
        public List<VocabEntry> Entries { get; set; } = new List<VocabEntry>();


        

    }
}
