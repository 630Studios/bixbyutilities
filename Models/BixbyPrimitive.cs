﻿namespace BixbyUtilities.Models
{
    public class BixbyPrimitive
    {
        public BixbyProperty Owner { get; set; }
        public string Name { get; set; }
        public BixbyTypes BixbyType { get; set; }

        public PrimitiveFeatureTypes Features { get; set; }

        public string Description { get; set; }
    }
}
