﻿using System.Reflection;
using BixbyUtilities.Convertors;


namespace BixbyUtilities.Extensions
{
    public static class PropertyInfoExtensions
    {
        public static BixbyTypes BixbyType(this PropertyInfo property)
        {
            System.Type propertyType = property.PropertyType;

            if (propertyType.IsClass && propertyType != typeof(string))
                return BixbyTypes.STRUCTURE;
            

            if (propertyType.IsArray)
                return BixbyTypes.ENUM;
                
            return TypeConversionTable.GetBixbyType(propertyType);
        }


        public static string GetBixbyStructureName(this PropertyInfo property)
        {
            return property.Name.ToUpper()[0] + property.Name.Substring(1);
        }

        public static string GetBixbyPropertyName(this PropertyInfo property)
        {
            return property.Name.ToLower()[0] + property.Name.Substring(1);
        }

        public static string GetBixbyPrimitiveName(this PropertyInfo property, string prefix = "")
        {
            return prefix + property.Name.ToUpper()[0] + property.Name.Substring(1);
        }

    }
}
