﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json.Linq;

namespace BixbyUtilities.Extensions
{
    public static class JTokenExtensions
    {

        public static BixbyTypes BixbyType(this JToken token)
        {
            switch (token.Type)
            {
                case JTokenType.Array:
                    return BixbyType(token.First);
                case JTokenType.Boolean:
                    return BixbyTypes.BOOLEAN;

                case JTokenType.Bytes:
                    break;
                case JTokenType.Date:
                    break;
                case JTokenType.Float:
                    return BixbyTypes.DECIMAL;

                case JTokenType.Integer:
                    return BixbyTypes.INTEGER;

                case JTokenType.Object:
                    return BixbyTypes.STRUCTURE;

                case JTokenType.String:
                    return BixbyTypes.NAME;

                case JTokenType.Uri:
                    return BixbyTypes.TEXT;

                default:
                    return BixbyTypes.UNDEFINED;
            }
            return BixbyTypes.UNDEFINED;
        }
    }
}
