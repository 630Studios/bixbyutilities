﻿using System.Collections.Generic;

namespace BixbyUtilities.Models
{
    public class BixbyStructure
    {
        public string Description { get; set; }
        public string Name { get; set; }

        public List<BixbyProperty> Properties { get; set; } = new List<BixbyProperty>();

        public StructureFeatureTypes Features { get; set; }
    }
}
