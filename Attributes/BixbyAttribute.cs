﻿using System;

namespace BixbyUtilities.Attributes
{

    /// <summary>
    /// Used to override the default export type.
    /// </summary>
    [System.AttributeUsage(System.AttributeTargets.Property)]
    public class BixbyTypeAttribute : Attribute
    {
        public BixbyTypes BixbyType = BixbyTypes.UNDEFINED;

        public BixbyTypeAttribute(BixbyTypes bixbyType)
        {
            this.BixbyType = bixbyType;
        }


    }

    /// <summary>
    /// Used to provide a description. Can be used on a Class or Property. When used on a property the description is applied to the derived primitive or structures description field.
    /// </summary>
    [System.AttributeUsage(System.AttributeTargets.Property | System.AttributeTargets.Class)]
    public class BixbyDescriptionAttribute : Attribute
    {
        public string Description;

        public BixbyDescriptionAttribute(string description)
        {
            this.Description = description;
        }


    }

    /// <summary>
    /// Used to specify features on the derived primitive of the property. On properties that do not generate a primitive this does nothing.
    /// </summary>
    [System.AttributeUsage(System.AttributeTargets.Property)]
    public class PrimitiveFeaturesAttribute : Attribute
    {

        public PrimitiveFeatureTypes Features;

        public PrimitiveFeaturesAttribute( PrimitiveFeatureTypes features)
        {
            
            this.Features = features;
        }

    }

    /// <summary>
    /// Used to specify features on the derived structure of a class or property. On properties that do not generate a structure this does nothing.
    /// </summary>
    [System.AttributeUsage(System.AttributeTargets.Property | System.AttributeTargets.Class)]
    public class StructureFeaturesAttribute : Attribute
    {

        public StructureFeatureTypes Features;

        public StructureFeaturesAttribute(StructureFeatureTypes features)
        {

            this.Features = features;
        }

    }


    /// <summary>
    /// Tells the convertor to ignore this class, or property.
    /// </summary>
    [System.AttributeUsage(System.AttributeTargets.Property | System.AttributeTargets.Class)]
    public class BixbyIgnore : Attribute
    {

    }

    /// <summary>
    /// Tells to convertor to mark this property as optional
    /// </summary>
    [System.AttributeUsage(System.AttributeTargets.Property)]
    public class BixbyOptional : Attribute
    {

    }

}
