﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json.Linq;

namespace BixbyUtilities.Extensions
{
    public static class JPropertyExtensions
    {


        public static BixbyTypes BixbyType(this JProperty property)
        {
            switch (property.Value.Type)
            {
                case JTokenType.Array:
                    return property.Value.First.BixbyType();
                case JTokenType.Boolean:
                    return BixbyTypes.BOOLEAN;
                    
                case JTokenType.Bytes:
                    break;
                case JTokenType.Date:
                    break;
                case JTokenType.Float:
                    return BixbyTypes.DECIMAL;

                case JTokenType.Integer:
                    return BixbyTypes.INTEGER;

                case JTokenType.Object:
                    return BixbyTypes.STRUCTURE;

                case JTokenType.String:
                    return BixbyTypes.NAME;

                case JTokenType.Uri:
                    return BixbyTypes.TEXT;
                    
                default:
                    return BixbyTypes.UNDEFINED;
            }
            return BixbyTypes.UNDEFINED;
        }

        

    
        public static string GetBixbyPropertyName(this JProperty property)
        {
            return property.Name.ToLower()[0] + property.Name.Substring(1);
        }

        public static string GetBixbyPrimitiveName(this JProperty property, string prefix = "")
        {
            return prefix + property.Name.ToUpper()[0] + property.Name.Substring(1);
        }

        public static string GetBixbyStructureName(this JProperty property)
        {
            return property.Name.ToUpper()[0] + property.Name.Substring(1);
        }

        public static PropertyMax GetBixbyPropertyMax(this JProperty property)
        {
            if (property.Value.Type == JTokenType.Array)
                return PropertyMax.Many;

            return PropertyMax.One;
        }
    }
}
