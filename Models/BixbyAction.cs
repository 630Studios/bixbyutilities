﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BixbyUtilities.Models
{
    public class BixbyAction
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public ActionTypes ActionType { get; set; }

    }
}
